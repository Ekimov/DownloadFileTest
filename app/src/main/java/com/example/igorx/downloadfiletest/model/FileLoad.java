package com.example.igorx.downloadfiletest.model;


public class FileLoad {

    String name;
    String description;
    String url;

    public int progress;
    public String download_status;
    public boolean isLoadingFile=false;

    public FileLoad(String name, String description, String url) {
     this.name=name;
     this.description=description;
     this.url=url;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
