package com.example.igorx.downloadfiletest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TMPActivity extends AppCompatActivity {


    ///this activity will delete
    //////////////
    //////////////
    private  static String TAG ="download_files";
    File fileDir;
    ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmp);
        initView();

       fileDir= new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"FIONA");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG,"onClick! ");
               new DownloadFile().execute("http://test.theawesome.click/Test_document_PDF.pdf");
            }
        });
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pb=(ProgressBar)findViewById(R.id.progressBar2);
        pb.setMax(100);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar2));
    }

public class DownloadFile extends AsyncTask<String,Integer,File>{
    URL url;
    HttpURLConnection urlConnection;
    InputStream inputStream;
    int totalSize;
    int downloadedSize;
    byte[] buffer;
    int bufferLength;
   // File file=null;
    FileOutputStream fos=null;
    private IOException m_error;

    @Override
    protected File doInBackground(String... params) {

        try {
            url = new URL(params[0]);
            urlConnection = (HttpURLConnection) url.openConnection();

//            urlConnection.setRequestMethod("GET");
//            urlConnection.setDoOutput(true);
            urlConnection.connect();

             File file = new File(Environment.getExternalStorageDirectory(),"PDF.pdf");
            fos = new FileOutputStream(file);
            inputStream = urlConnection.getInputStream();

            totalSize = urlConnection.getContentLength();
            downloadedSize = 0;

            buffer = new byte[1024];
            bufferLength = 0;

            // читаем со входа и пишем в выход,
            // с каждой итерацией публикуем прогресс
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                publishProgress(downloadedSize, totalSize);
            }

            fos.close();
            inputStream.close();

            return file;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            m_error = e;
        } catch (IOException e) {
            e.printStackTrace();
            m_error = e;
        }

        return null;
    }
    protected void onProgressUpdate(Integer... values) {
        Log.i(TAG,"ProgressUpdate "+values);
        pb.setProgress((int) ((values[0] / (float) values[1]) * 100));
    };

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        if (m_error != null) {
            Log.i(TAG,"m_error onPostExecute  "+m_error.getMessage());
            m_error.printStackTrace();
            return;
        }
        // закрываем прогресс и удаляем временный файл

       // file.delete();
    }



}

}
