package com.example.igorx.downloadfiletest.model;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ProgressBar;

import com.example.igorx.downloadfiletest.R;
import com.example.igorx.downloadfiletest.adapter.FileAdapter;
import com.example.igorx.downloadfiletest.util.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class DownloadFileTask extends AsyncTask<String,Integer,File> {

    //ProgressBar pb;
    FileLoad fileLoad;
    FileAdapter fileAdapter;
    //String nameFile;
    URL url;
    HttpURLConnection urlConnection;
    InputStream inputStream;
    int totalSize;
    int downloadedSize;
    byte[] buffer;
    int bufferLength;
    // File file=null;
    FileOutputStream fos=null;
    private IOException m_error;


    public  DownloadFileTask(FileLoad fileLoad, FileAdapter fileAdapter) {
        //this.pb=pb;
        this.fileLoad=fileLoad;
        this.fileAdapter=fileAdapter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        fileLoad.download_status=Constants.LOADING;
        fileAdapter.notifyDataSetChanged();
        Log.i(Constants.TAG, "AsynTask start, name =  " + fileLoad.getName());
    }

    @Override
    protected File doInBackground(String... params) {

        try {
            url = new URL(params[0]);
            urlConnection = (HttpURLConnection) url.openConnection();


            urlConnection.connect();

            File file = new File(Environment.getExternalStorageDirectory(),fileLoad.getUrl());
            fos = new FileOutputStream(file);
            inputStream = urlConnection.getInputStream();

            totalSize = urlConnection.getContentLength();
            downloadedSize = 0;

            buffer = new byte[1024];
            bufferLength = 0;


            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                publishProgress(downloadedSize, totalSize);
            }

            fos.close();
            inputStream.close();

            return file;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            m_error = e;
        } catch (IOException e) {
            e.printStackTrace();
            m_error = e;
        }

        return null;
    }
    protected void onProgressUpdate(Integer... values) {
        //Log.i(TAG, "ProgressUpdate " + values);
        fileLoad.progress=(int) ((values[0] / (float) values[1]) * 100);
        fileAdapter.notifyDataSetChanged();
    };

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        if (m_error != null) {
            fileLoad.download_status=Constants.ERROR_LOADING;
            fileAdapter.notifyDataSetChanged();
            Log.i(Constants.TAG,"Error onPostExecute  "+m_error.getMessage()+fileLoad.getName());
            m_error.printStackTrace();
            return;
        }
        fileLoad.download_status=Constants.DOWNLOADED;
        fileAdapter.notifyDataSetChanged();
        Log.i(Constants.TAG, "AsynTask stop, name =  "+fileLoad.getName());

    }
}
