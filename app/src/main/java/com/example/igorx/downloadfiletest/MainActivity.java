package com.example.igorx.downloadfiletest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.igorx.downloadfiletest.adapter.FileAdapter;
import com.example.igorx.downloadfiletest.model.API;
import com.example.igorx.downloadfiletest.model.DownloadFileTask;
import com.example.igorx.downloadfiletest.model.FileLoad;
import com.example.igorx.downloadfiletest.util.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {


   private API api;
   private ArrayList<FileLoad> fileLoads;
    private ListView lv;
    private ProgressBar getFileProgress;
    private FileAdapter adapter;
    private boolean isInternetConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isInternetConnection=isNetworkConnectedOrConnecting();
        Log.i(Constants.TAG,"isInternetConnection = "+isInternetConnection);
        initView();
        fileLoads=new ArrayList();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://test.theawesome.click")
                .build();

        api = restAdapter.create(API.class);


        if(isInternetConnection){
            getFileDownload();
        }
        else {
            getFileProgress.setVisibility(View.GONE);
            Toast.makeText(this,"No Internet connection",Toast.LENGTH_LONG).show();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getFileDownload();

            }
        });
    }

    private void initView() {
        getFileProgress=(ProgressBar)findViewById(R.id.progress_file);
        lv=(ListView)findViewById(R.id.lvFiles);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void getFileDownload() {
       api.getResponse(new Callback<Response>() {
           @Override
           public void success(Response response, Response response2) {

               String stringResponse = stringFromResponse(response);
               Log.i(Constants.TAG, "Success, response = " + stringResponse);
               try {
                   JSONObject files = new JSONObject(stringResponse);
                   JSONArray fileArray = files.getJSONArray("file_list");
                   for (int i = 0; i < fileArray.length(); i++) {
                       String name = fileArray.getJSONObject(i).getString("name");
                       String discription = fileArray.getJSONObject(i).getString("description");
                       String url = fileArray.getJSONObject(i).getString("url");

                       fileLoads.add(new FileLoad(name, discription, url));

                   }
                   downLoadList();
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }

           @Override
           public void failure(RetrofitError error) {
               Log.i(Constants.TAG, "failure " + error.getMessage());
           }
       });
    }

    private void downLoadList() {
        getFileProgress.setVisibility(View.GONE);
        lv.setVisibility(View.VISIBLE);
        adapter= new FileAdapter(this,fileLoads);
        lv.setAdapter(adapter);
        for(int i=0; i<fileLoads.size();i++){

                new DownloadFileTask(fileLoads.get(i),adapter).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,""+ Constants.URL+fileLoads.get(i).getUrl());

        }
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    String stringFromResponse(Response response){
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(
                    response.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        String result = sb.toString();
        return result;
    }


    public boolean isNetworkConnectedOrConnecting() {
        final ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }
}
