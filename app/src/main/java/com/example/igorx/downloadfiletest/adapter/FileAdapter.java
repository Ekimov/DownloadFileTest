package com.example.igorx.downloadfiletest.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.igorx.downloadfiletest.R;
import com.example.igorx.downloadfiletest.model.DownloadFileTask;
import com.example.igorx.downloadfiletest.model.FileLoad;
import com.example.igorx.downloadfiletest.util.Constants;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class FileAdapter extends BaseAdapter {

   private ArrayList<FileLoad> fileLoads;
   private Context context;
   private  View mView;
   private ViewHolder viewHolder;
   public  static String URL = "http://test.theawesome.click/";
     public  FileAdapter(Context context,ArrayList<FileLoad> fileLoads) {

       this.context=context;
       this.fileLoads=fileLoads;

     }

    @Override
    public int getCount() {
        return fileLoads.size();
    }

    @Override
    public Object getItem(int position) {
        return fileLoads.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        mView=convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView==null) {
            viewHolder=new ViewHolder();
            mView = inflater.inflate(R.layout.item_files, null);
            viewHolder.tvName=(TextView)mView.findViewById(R.id.name);
            viewHolder.tvDescription=(TextView)mView.findViewById(R.id.discription);
            viewHolder.progressBar=(ProgressBar)mView.findViewById(R.id.progressBar);
            viewHolder.tvStatus=(TextView)mView.findViewById(R.id.tvStatus);
           // viewHolder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.progress_bar2));
            mView.setTag(viewHolder);
        }
        viewHolder= (ViewHolder) mView.getTag();
        viewHolder.tvName.setText(fileLoads.get(position).getName());
        viewHolder.tvDescription.setText(fileLoads.get(position).getDescription());
        viewHolder.progressBar.setProgress(fileLoads.get(position).progress);
        viewHolder.tvStatus.setText(fileLoads.get(position).download_status);

//           if(fileLoads.get(position).download_status==Constants.LOADING) {
//
//               viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_loading));
//           }
//
//
//        if(fileLoads.get(position).download_status==Constants.DOWNLOADED) {
//
//            viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_downloaded));
//        }
//         if(fileLoads.get(position).download_status==Constants.ERROR_LOADING){
//
//             viewHolder.tvStatus.setText(context.getResources().getString(R.string.status_error_loading));
//         }
//        if(file.isLoadingFile){
//            new DownloadFileTask(viewHolder.progressBar,file.getName()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,""+URL+file.getUrl());
//            file.isLoadingFile=false;
//        }
        return mView;
    }

    public static class ViewHolder{
       private TextView tvName;
       private TextView tvStatus;
       private TextView tvDescription;
       private ProgressBar progressBar;
    }
}
