package com.example.igorx.downloadfiletest.model;


import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;

public interface API
{
    @GET("/backend.json")
    void getResponse(Callback<Response> cb);
}
