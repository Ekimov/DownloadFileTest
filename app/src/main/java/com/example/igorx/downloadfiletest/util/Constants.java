package com.example.igorx.downloadfiletest.util;


public class Constants {
    public static String TAG ="download_files";
    public static String URL = "http://test.theawesome.click/";
    public static String NOT_DOWNLOADED ="Not downloaded";
    public static String LOADING ="Loading";
    public static String ERROR_LOADING ="Error loading";
    public static String DOWNLOADED ="Downloaded";

}
